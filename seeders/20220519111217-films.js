'use strict';
const fs = require('fs');

module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */

    const data = JSON.parse(fs.readFileSync('./data.json', 'utf-8'))
    const films = data['Films'].map((element) => {
      return {
        title: element.title,
        genre: element.genre,
        sinopsis: element.sinopsis,
        director: element.director,
        writer: element.writer,
        releaseYear: element.releaseYear,
        createdAt: new Date(),
        updatedAt: new Date(),
      }
    })
    
    return queryInterface.bulkInsert('Films', films);
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('Films', null, { truncate: true, restartIdentity: true });
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
