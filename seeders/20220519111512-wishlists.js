'use strict';
const fs = require('fs');

module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */

    const data = JSON.parse(fs.readFileSync('./data.json', 'utf-8'))
    const wishlists = data['Wishlists'].map((element) => {
      return {
        userId: element.userId,
        filmId: element.filmId,
        createdAt: new Date(),
        updatedAt: new Date(),
      }
    })
    
    return queryInterface.bulkInsert('Wishlists', wishlists);
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('Wishlists', null, { truncate: true, restartIdentity: true });
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
