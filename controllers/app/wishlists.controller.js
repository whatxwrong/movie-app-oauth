const { Wishlists, Films } = require('../../models')

class WishlistsController {
  static async list(req, res, next) {
    try {
      console.log(req.users)
      const wishlists = await Wishlists.findAll({
        where: {
          userId: req.user.id
        },
        attributes: [],
        include: {
          model: Films,
          as: "Film",
          attributes: ['title', 'genre', 'sinopsis', 'director', 'writer', 'releaseYear']
        }
      })
      
      res.status(200).json(wishlists)
    } catch (err) {
      next(err)
    }
  }

  static async create(req, res, next) {
    try {
      await Wishlists.create({
        userId: req.user.id,
        filmId: req.body.filmId
      })

      res.status(201).json({
        message: 'Successfully create wishlist'
      })
    } catch (err) {
      
    }
  }

  static async delete(req, res, next) {
    try {
      const wishlists = await Wishlists.findOne({
        where: {
          id: req.params.id
        }
      })
      
      if (!wishlists) {
        throw {
          status: 404,
          message: 'Wishlist not found'
        }
      } else {
        if (wishlists.userId !== req.user.id) {
          throw {
            status: 401,
            message: 'Unauthorized request'
          }
        } else {
          await Wishlists.destroy({
            where: {
              id: req.params.id
            } 
          })

          res.status(200).json({
            message: 'Successfully delete wishlist'
          })
        }
      }

    } catch (err) {
      next(err)
    }
  }
}

module.exports = WishlistsController