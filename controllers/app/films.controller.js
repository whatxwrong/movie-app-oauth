const { Films } = require('../../models')

class FilmsController {
  static async list (req, res, next) {
    try {
      const films = await Films.findAll({
        attributes: ['id', 'title', 'genre']
      })
      res.status(200).json(films)
    } catch (err) {
      next(err)
    }
  }

  static async getById(req, res, next) {
    try {
      const films = await Films.findOne({
        where: {
          id: req.params.id
        }
      })

      if (!films) {
        throw {
          status: 404,
          message: 'Film Not Found'
        }
      } else {
        res.status(200).json({
          title: films.title,
          genre: films.genre,
          sinopsis: films.sinopsis,
          director: films.director,
          writer: films.writer,
          releaseYear: films.releaseYear,
          poster: films.poster
        })
      }
    } catch (err) {
      next(err)
    }
  }
}

module.exports = FilmsController