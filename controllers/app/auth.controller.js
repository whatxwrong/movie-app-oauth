const { Users } = require('../../models')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const { OAuth2Client } = require('google-auth-library')
const axios = require('axios')
const client = new OAuth2Client(process.env.GOOGLE_CLIENT_ID, process.env.GOOGLE_CLIENT_SECRET)

class AuthController {
  static async LoginPassword(req, res, next) {
    try {
      if (req.body.email && req.body.password) {
        const user = await Users.findOne({
          where: {
            email: req.body.email
          }
        })
  
        if (!user) {
          throw {
            status: 401,
            message: 'Invalid email or password'
          }
        }
        if (bcrypt.compareSync(req.body.password, user.password)) {
          const token = jwt.sign({
            id: user.id,
            email: user.email
          }, process.env.MY_JWT_TOKEN)
  
          res.status(200).json({
            token
          })
        } else {
          throw {
            status: 401,
            message: 'Invalid email or password'
          }
        }
      }
    } catch (err) {
      next(err)
    }
  }

  static async GoogleLogin(req, res, next) {
    try {
      // melakukan verifikasi id token
      const payload = await client.verifyIdToken({
        idToken: req.body.google_id_token,
        audience: process.env.GOOGLE_CLIENT_IDs
      })

      console.log(payload.payload.email)
      // mencari email dari google di database
      const user = await Users.findOne({
        where: {
          email: payload.payload.email
        }
      })

      if (user) {
        const token = jwt.sign({
          id: user.id,
          email: user.email
        }, process.env.MY_JWT_TOKEN)
        res.status(200).json({ token })
      } else {
        const createdUser = await Users.create({
          email: payload.payload.email
        })
        const token = jwt.sign({
          id: createdUser.id,
          email: createdUser.email
        }, process.env.MY_JWT_TOKEN)
        res.status(200).json({ token })
      }
      // mendaftarkan secara otomatis, jika user belom ada di database
    } catch (err) {
      next(err)
    }
  }
  
  static async FacebookLogin(req, res, next) {
    try {
      const response = await axios.get(`https://graph.facebook.com/v13.0/me?fields=id%2Cname%2Cemail%2Cgender%2Cbirthday&access_token=${req.body.facebook_id_token}`)
      console.log(response.data)
      // mencari user di database
      const user = await Users.findOne({
        where: {
          email: response.data.email
        }
      })
      
      if (user) {
        const token = jwt.sign({
          id: user.id,
          email: user.email
        }, process.env.MY_JWT_TOKEN)
        
        res.status(200).json({ token })
      } else {
        const createdUser = await Users.create({
          email: response.data.email
        })
        
        const token = jwt.sign({
          id: createdUser.id,
          email: createdUser.email
        }, process.env.MY_JWT_TOKEN)
        res.status(200).json({ token })
      }
    } catch (err) {
      next(err)
    }
  }

  static async register(req, res, next) {
    try {
      await Users.create({
        email: req.body.email,
        password: req.body.password
      })
      res.status(201).json({
        message: 'Register Succesfully'
      })
    } catch (err) {
      next(err)
    }
  }
}

module.exports = AuthController