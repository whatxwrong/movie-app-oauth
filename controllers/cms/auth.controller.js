const { Admin, Users } = require('../../models')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')

class AuthController {
  static async LoginPassword(req, res, next) {
    try {
      const admin = await Admin.findOne({
        where: {
          email: req.body.email
        }
      })
      
      if (!admin) {
        throw {
          status: 401,
          message: "Invalid email or password"    
        }
      }
      
      if (bcrypt.compareSync(req.body.password, admin.password)) {
        const token = jwt.sign({
          id: admin.id,
          email: admin.email
        }, process.env.MY_JWT_TOKEN)

        res.status(200).json({
          token
        })
      } else {
        throw {
          status: 401,
          message: "Invalid email or password"
        }
      }
    } catch(err) {
      next(err)
    }
  }

  static async create(req, res, next) {
    try {
      const user = await Users.findOne({
        where: {
          email: req.body.email
        }
      })

      if (user) {
        throw {
          status: 401,
          message: "This Email is already taken"
        }
      } else {
        const admin = await Admin.findOne({
          where: {
            email: req.body.email
          }
        })
        
        if (admin) {
          throw {
            status: 401,
            message: "This Email is already taken"
          }
        } else {
          const admin = await Admin.findOne({
            where: {
              email: req.admin.email
            }
          })
          if (admin.role != 'superadmin') {
            throw {
              status: 401,
              message: "Unauthorized request"
            }
          } else {
            await Admin.create({
              email: req.body.email,
              password: req.body.password,
              role: req.body.role
            })
            res.status(201).json({
              message: 'Register Succesfully'
            })
          }
        }
      }
    } catch (err) {
      next(err)
    }
  }
}

module.exports = AuthController