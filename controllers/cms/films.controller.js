const { Films } = require('../../models')
const fs = require('fs')

class FilmsController {
  static async list (req, res, next) {
    try {
      const films = await Films.findAll({
        attributes: ['id', 'title', 'genre']
      })
      res.status(200).json(films)
    } catch (err) {
      next(err)
    }
  }

  static async getById(req, res, next) {
    try {
      const films = await Films.findOne({
        where: {
          id: req.params.id
        }
      })

      if (!films) {
        throw {
          status: 404,
          message: 'Film Not Found'
        }
      } else {
        res.status(200).json({
          title: films.title,
          genre: films.genre,
          sinopsis: films.sinopsis,
          director: films.director,
          writer: films.writer,
          releaseYear: films.releaseYear,
          poster: films.poster
        })
      }
    } catch (err) {
      next(err)
    }
  }

  static async create(req, res, next) {
    try {
      if (req.file) {
        req.body.poster = `http://localhost:3000/${req.file.filename}`
      }
      await Films.create({
        title: req.body.title,
        genre: req.body.genre,
        sinopsis: req.body.sinopsis,
        director: req.body.director,
        writer: req.body.writer,
        releaseYear: req.body.releaseYear,
        poster: req.body.poster
      })
      res.status(201).json({
        message: "Successfully Create Films"
      })
    } catch(err) {
      next(err)
    }
  }
  
  static async update(req, res, next) {
    try {
      const films = await Films.findOne({
        where: {
          id: req.params.id
        }
      })

      if (!films) {
        throw {
          status: 404,
          message: 'Film Not Found'
        }
      } else {        
        if (req.file) {
          req.body.poster = `http://localhost:3000/${req.file.filename}`
        }

        // Proccess Upload Data
        await Films.update({
          title: req.body.title,
          genre: req.body.genre,
          sinopsis: req.body.sinopsis,
          director: req.body.director,
          writer: req.body.writer,
          releaseYear: req.body.releaseYear,
          poster: req.body.poster
        }, {
          where: {
            id: req.params.id
          }
        })

        // Delete File
        const DIR = 'public/src/' + films.poster.split('http://localhost:3000/')[1]
        if (fs.existsSync(DIR)) {
          fs.unlinkSync(DIR)
        }

        res.status(201).json({
          message: "Successfully Update Films"
        })
      }
    } catch(err) {
      next(err)
    }
  }
  
  static async delete(req, res, next) {
    try {
      const films = await Films.findOne({
        where: {
          id: req.params.id
        }
      })

      if (!films) {
        throw {
          status: 404,
          message: "Films not found!"
        }
      } else {
        await Films.destroy({
          where: {
            id: req.params.id
          }
        })
        res.status(200).json({
          message: "Successfully deleted"
        })
      }
    } catch(err) {
      next(err)
    }
  }
}

module.exports = FilmsController