const express = require('express')
const app = express()
const port = 3000
const routes = require('./routes/index.route.js')
const errorHandler = require('./middlewares/errorHandler')
const morgan = require('morgan')
require('dotenv').config()

app.use(express.urlencoded({ extended: false }))
app.use(express.json())
app.use(morgan('dev'))

app.use(express.static('public/src'))
app.use(routes)
app.use(errorHandler)

// app.listen(port, () => {
//   console.log(`Example app listening on port ${port}`)
// })

module.exports = app
