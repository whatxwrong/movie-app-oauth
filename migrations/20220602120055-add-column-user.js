'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.addColumn('Films', 'poster', { type: Sequelize.STRING });
  },
  
  async down (queryInterface, Sequelize) {
    await queryInterface.addColumn('Films', 'poster', { /* query options*/ });
  }
};
