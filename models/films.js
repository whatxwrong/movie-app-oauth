'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Films extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Films.hasMany(models.Wishlists, { foreignKey: 'filmId', as: 'Films' })
    }
  }
  Films.init({
    title: DataTypes.STRING,
    genre: DataTypes.STRING,
    sinopsis: DataTypes.STRING,
    director: DataTypes.STRING,
    writer: DataTypes.STRING,
    releaseYear: DataTypes.INTEGER,
    poster: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Films',
  });
  return Films;
};