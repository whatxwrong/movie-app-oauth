'use strict';
const {
  Model
} = require('sequelize');

const bcrypt = require('bcryptjs')

module.exports = (sequelize, DataTypes) => {
  class Admin extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  Admin.init({
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    role: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Admin',
  });
  Admin.addHook('beforeCreate', (admin, options) => {
    if (admin.password) {
      const salt = bcrypt.genSaltSync(10);
      const hash = bcrypt.hashSync(admin.password, salt);
      admin.password = hash;
    } else {
      admin.password = '';
    }
  });
  return Admin;
};