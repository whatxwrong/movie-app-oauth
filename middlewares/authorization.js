const { Users, Admin } = require('../models')
const jwt = require('jsonwebtoken')

const AdminAuthorization = async (req, res, next) => {
  try {
    if (!req.headers.authorization) {
      throw {
        status: 401,
        message: 'Unauthorized request'
      }
    } else {
      const admin = jwt.verify(req.headers.authorization, process.env.MY_JWT_TOKEN)
      if (admin) {
        // mencari adminnya di database
        const loggedInAdmin = await Admin.findOne({
          where: {
            email: admin.email
          }
        })
        if (loggedInAdmin) {
          req.admin = admin
          next()
        } else {
          throw {
            status: 401,
            message: 'Unauthorized request'
          }
        }
      } else {
        throw {
          status: 401,
          message: 'Unauthorized request'
        }
      }
    }
  } catch(err) {
    next(err)
  }
}

const UserAuthorization = async (req, res, next) => {
  try {
    if (!req.headers.authorization) {
      throw {
        status: 401,
        message: 'Unauthorized request'
      }
    } else {
      const users = jwt.verify(req.headers.authorization, process.env.MY_JWT_TOKEN)
      console.log(users)
      if (users) {
        // mencari usersnya di database
        const loggedInUsers = await Users.findOne({
          where: {
            email: users.email
          }
        })
        if (loggedInUsers) {
          req.user = users
          next()
        } else {
          throw {
            status: 401,
            message: 'Unauthorized request'
          }
        }
      } else {
        throw {
          status: 401,
          message: 'Unauthorized request'
        }
      }
    }
  } catch(err) {
    next(err)
  }
}

module.exports = {
  AdminAuthorization,
  UserAuthorization,
}