const express = require('express')
const router = express.Router()
const jwt = require('jsonwebtoken')
const WishlistsController = require('../../controllers/app/wishlists.controller')
const { Users } = require('../../models')
const { UserAuthorization } = require('../../middlewares/authorization')

router.get('/', UserAuthorization, WishlistsController.list)

router.post('/', UserAuthorization,
  (req, res, next) => {
    const errors = []
    if (!req.body.filmId) {
      errors.push('FilmId is required')
    }
  
    if (errors.length > 0) {
      next({
        status: 400,
        message: errors
      })
    } else {
      next()
    }
  },
WishlistsController.create)

router.delete('/:id', UserAuthorization, WishlistsController.delete)

module.exports = router