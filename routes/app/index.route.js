const express = require('express')
const router = express.Router()
const authRoutes = require('./auth.route')
const filmRoutes = require('./films.route')
const wishlistRoutes = require('./wishlists.route')

router.use('/auth', authRoutes)
router.use('/films', filmRoutes)
router.use('/wishlists', wishlistRoutes)

module.exports = router