const express = require('express')
const router = express.Router()
// const jwt = require('jsonwebtoken')
const FilmsController = require('../../controllers/app/films.controller')
// const { Films } = require('../models')

router.get('/', FilmsController.list)
router.get('/:id', FilmsController.getById)

module.exports = router