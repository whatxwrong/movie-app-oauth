const express = require('express')
const router = express.Router()
const AuthController = require('../../controllers/app/auth.controller')

// Login by Email & Password
router.post('/login-password',
  (req, res, next) => {
    const errors = []
    if (!req.body.email) {
      errors.push('Email is required')
    }
    if (!req.body.password) {
      errors.push('Password is required')
    }
  
    if (errors.length > 0) {
      next({
        status: 400,
        message: errors
      })
    } else {
      next()
    }
  },
AuthController.LoginPassword)

// Login by Facebook
router.post('/login-google', 
  (req, res, next) => {
    const errors = []
    if (!req.body.google_id_token) {
      errors.push('Google Id Token is required')
    }
  
    if (errors.length > 0) {
      next({
        status: 400,
        message: errors
      })
    } else {
      next()
    }
  },
AuthController.GoogleLogin)

// Login by Facebook
router.post('/login-facebook', 
  (req, res, next) => {
    const errors = []
    if (!req.body.facebook_id_token) {
      errors.push('Facebook Id Token is required')
    }
  
    if (errors.length > 0) {
      next({
        status: 400,
        message: errors
      })
    } else {
      next()
    }
  },
AuthController.FacebookLogin)

// Daftar By Password
router.post('/register', AuthController.register)

module.exports = router