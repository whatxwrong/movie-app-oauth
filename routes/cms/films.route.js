const express = require('express')
const router = express.Router()
const jwt = require('jsonwebtoken')
const FilmsController = require('../../controllers/cms/films.controller')
const { AdminAuthorization } = require('../../middlewares/authorization')
const multer = require('multer')
const storage = require('../../services/multerStorage.service')
const upload = multer({
  storage,
  limits: {
    fileSize: 3000000
  },
  fileFilter: (req, file, cb) => {
    if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png' || file.mimetype === 'image/jpg') {
      cb(null, true)
    } else {
      cb({
        status: 400,
        message: 'File type does not match'
      }, false)
    }
  }
})

router.get('/', FilmsController.list)
router.get('/:id', FilmsController.getById)
router.post('/', AdminAuthorization, upload.single('poster'), 
  (req, res, next) => {
    const errors = []
    if (!req.body.title) {
      errors.push('Title is required')
    }
    if (!req.body.genre) {
      errors.push('Genre is required')
    }
    if (!req.body.sinopsis) {
      errors.push('Sinopsis is required')
    }
    if (!req.body.director) {
      errors.push('Director is required')
    }
    if (!req.body.writer) {
      errors.push('Writer is required')
    }
    if (!req.body.releaseYear) {
      errors.push('Release Year is required')
    }
  
    if (errors.length > 0) {
      next({
        status: 400,
        message: errors
      })
    } else {
      next()
    }
  },
FilmsController.create)
router.put('/:id', AdminAuthorization, upload.single('poster'), 
  (req, res, next) => {
    const errors = []
    if (req.body.title !== undefined) {
      if (!req.body.title) {
        errors.push('Title is required')
      }
    }
    if (req.body.genre !== undefined) {
      if (!req.body.genre) {
        errors.push('Genre is required')
      }
    }
    if (req.body.sinopsis !== undefined) {
      if (!req.body.sinopsis) {
        errors.push('Sinopsis is required')
      }
    }
    if (req.body.director !== undefined) {
      if (!req.body.director) {
        errors.push('Director is required')
      }
    }
    if (req.body.writer !== undefined) {
      if (!req.body.writer) {
        errors.push('Writer is required')
      }
    }
    if (req.body.releaseYear !== undefined) {
      if (!req.body.releaseYear) {
        errors.push('Release Year is required')
      }
    }
  
    if (errors.length > 0) {
      next({
        status: 400,
        message: errors
      })
    } else {
      next()
    }
  },
FilmsController.update)
router.delete('/:id', AdminAuthorization, FilmsController.delete)

module.exports = router