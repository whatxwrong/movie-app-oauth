const express = require('express')
const router = express.Router()
const jwt = require('jsonwebtoken')
const AuthController = require('../../controllers/cms/auth.controller')
const { AdminAuthorization } = require('../../middlewares/authorization')

router.post('/login', AuthController.LoginPassword)
router.post('/register', AdminAuthorization, AuthController.create)

module.exports = router